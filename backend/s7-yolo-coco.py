
import sys
import tqdm
import uuid
import time
import os
import cv2
import numpy as np
import glob
from PIL import Image

# darknet_path = "/Users/annabaydina/Documents/s7/s7-mipt-dev-course/darknet"
# sys.path.append(darknet_path)
# import darknet


class YOLO(object):
    metaMain = None
    netMain = None
    altNames = None
    net_size = None
    darknet_image = None

    def predict(self, image, nms=0.6, darknet_flag=False):
        height, width, _ = image.shape
        image = cv2.resize(image, self.net_size, interpolation=cv2.INTER_LINEAR)
        
        if darknet_flag:

            darknet.copy_image_from_bytes(self.darknet_image, image.tobytes())
            detections = np.array(
                darknet.detect_image(self.netMain, self.metaMain, self.darknet_image, thresh=0.1, nms=nms))
            res = []
            if len(detections) > 0:
                detections[:, 0] = detections[:, 0].astype('U13')
                if len(detections) > 0:
                    classes = detections[:, 0]
                    confidence = detections[:, 1]
                    boxes = np.array(detections[:, 2])
                    boxes = np.array(list(boxes))
                    boxes[:, ::2] *= width / self.net_size[0]
                    boxes[:, 1::2] *= height / self.net_size[1]
                    res = np.column_stack((boxes, confidence, classes))
            return res
        else:
            return None

    def __init__(self, model_path, weights_path, darknet_flag=False):

        print(model_path)
        self.configPath = glob.glob(f'{model_path}/*.cfg')[0]
        self.weightPath = glob.glob(f'{weights_path}.weights')[0]
        self.metaPath = glob.glob(f'{model_path}/*.data')[0]
        self.namesPath = glob.glob(f'{model_path}/*.names')[0]

        if not os.path.exists(self.configPath):
            raise ValueError("Invalid config path `" +
                             os.path.abspath(self.configPath) + "`")

        if not os.path.exists(self.weightPath):
            raise ValueError("Invalid weight path `" +
                             os.path.abspath(self.weightPath) + "`")

        if not os.path.exists(self.metaPath):
            raise ValueError("Invalid data file path `" +
                             os.path.abspath(self.metaPath) + "`")

        if not os.path.exists(self.namesPath):
            raise ValueError("Invalid name file path `" +
                             os.path.abspath(self.namesPath) + "`")

        if self.altNames is None:
            found = False
            with open(self.metaPath) as f:
                data = f.read()

            with open(self.metaPath, 'w') as f:
                for l in data.splitlines():
                    if l.startswith('names'):
                        found = True
                        pth = l.split('=')[-1]
                        if not os.path.isfile(pth):
                            l = f'names={self.namesPath}'

                    f.write(l + '\n')
                if not found:
                    f.write(f'names={self.namesPath}' + '\n')

            with open(self.namesPath) as namesFH:
                namesList = namesFH.read().strip().split("\n")
                self.altNames = [x.strip() for x in namesList]
        print('Yolo ready (without darknet)')

        if darknet_flag:
            if self.netMain is None:
                self.netMain = darknet.load_net_custom(self.configPath.encode(
                    "ascii"), self.weightPath.encode("ascii"), 0, 1)  # batch size = 1

            if self.metaMain is None:
                self.metaMain = darknet.load_meta(self.metaPath.encode("ascii"))

            if self.net_size is None:
                self.net_size = (darknet.network_width(self.netMain), darknet.network_height(self.netMain))

            if self.darknet_image is None:
                self.darknet_image = darknet.make_image(self.net_size[0], self.net_size[1], 3)
        


def convertBack(row):
    # для рисования рамочек на картинках
    x, y, w, h, conf, cl = row
    xmin = x - (w // 2) if x - (w // 2) >= 1 else 1
    xmax = x + (w // 2) if x + (w // 2) >= 1 else 1
    ymin = y - (h // 2) if y - (h // 2) >= 1 else 1
    ymax = y + (h // 2) if y + (h // 2) >= 1 else 1
    return [int(xmin), int(ymin), int(xmax), int(ymax), float(conf), str(cl)]

        
def run_pictures(yolo:YOLO, picture, darknet_flag=False):

    frame = cv2.imread(picture)

    height, width = frame.shape[0], frame.shape[1]
    
    if darknet_flag:
        dets = yolo.predict(frame)

        detections = np.apply_along_axis(convertBack, 1, dets) if len(dets)>0 else []

        for i,det in enumerate(detections):

            x,y,w,h,conf,cl = det
            x,y,w,h,conf,cl = int(x), int(y), int(w), int(h), float(conf), str(cl)

            if conf > 0.5:
                frame = cv2.rectangle(frame, (x, y), (w, h), (0,255,0), 2)
                cv2.putText(frame, str(cl), (x, y-10), cv2.FONT_HERSHEY_COMPLEX, 0.9, (0,255,0), 2)
            
    else:
        print("I'm working, but no darknet found")
        cv2.putText(frame, f'No darknet found, sosi hui', 
                    (100, 100), cv2.FONT_HERSHEY_COMPLEX, 0.9, (0,255,0), 2)
    return Image.fromarray(frame)
    
            

yolo = YOLO('/Users/annabaydina/Documents/s7/s7-mipt-dev-course/yolo', 
         '/Users/annabaydina/Documents/s7/s7-mipt-dev-course/yolo/yolov4-coco')

run_pictures(yolo, '/Users/annabaydina/Documents/s7/s7-mipt-dev-course/yolo/ksi.jpg')






