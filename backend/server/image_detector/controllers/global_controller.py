from typing import List, Dict
from aiohttp import web

from image_detector.models.api_error import ApiError
from image_detector import util


async def get_detections(request: web.Request, body=None) -> web.Response:
    """detect bounding boxes

    

    :param body: Image that needs to be processed by YOLO
    :type body: str

    """
    return web.Response(status=200)
