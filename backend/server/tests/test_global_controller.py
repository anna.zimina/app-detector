# coding: utf-8
from io import BytesIO

import pytest
import json
from aiohttp import web

from image_detector.models.api_error import ApiError


# @pytest.mark.skip("image/jpeg not supported by Connexion")
async def test_get_detections(client):
    """Test case for get_detections

    detect bounding boxes
    """
    body = (BytesIO(b'some file data'), 'ksi.jpg')
    headers = { 
        'Accept': 'application/json',
        'Content-Type': 'image/jpeg',
        'Authorization': 'Bearer special-key',
    }
    response = await client.request(
        method='POST',
        path='/api/imageDetector/v1/detect',
        headers=headers,
        json=body,
        )
    assert response.status == 200, 'Response body is : ' + (await response.read()).decode('utf-8')

